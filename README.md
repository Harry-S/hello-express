# Hello-express

This project contains REST API with following features:

- Welcome endpoint
- Multiply endpoint

## Getting started

Fist install the dependencies

```sh
npm i
```

Start the REST API service:

`node app.js`

Or in the lates NodeJS version (>=20):

`node --watch app.js`

## Gitignore
**Make sure to include node_modules in the .gitignore file.**
![Alt text](image.png)